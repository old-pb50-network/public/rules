module.exports = {
    title: 'PB50 Network Rules',
    description: 'Rules and Punishments for PB50 Network',
    dest: 'public',
    themeConfig: {
        search: false,
        lastUpdated: 'Last Updated',
        smoothScroll: true,
        sidebar: [
            '/',
        ]
    }
};