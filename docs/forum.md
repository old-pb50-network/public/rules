# Forum Rules
## 1. No Spam / Advertising / Self-promote in the forums
These forums define spam as unsolicited advertisement for goods, services and/or other web sites, or posts with little, or completely unrelated content. Do not spam the forums with links to your site or product, or try to self-promote your website, business or forums etc.
Spamming also includes sending private messages to a large number of different users.
**DO NOT ASK for email addresses or phone numbers**

Your account will be banned permanently and your posts will be deleted.
## 2. Do not post copyright-infringing material
Providing or asking for information on how to illegally obtain copyrighted materials is forbidden.
## 3. Do not post “offensive” posts, links or images
Any material which constitutes defamation, harassment, or abuse is strictly prohibited. Material that is sexually or otherwise obscene, racist, or otherwise overly discriminatory is not permitted on these forums. This includes user pictures. Use common sense while posting.
## 4. Do not cross post questions
Please refrain from posting the same question in several forums. There is normally one forum which is most suitable in which to post your question.
## 5. Do not PM users asking for help
Do not send private messages to any users asking for help. If you need help, make a new thread in the appropriate forum then the whole community can help and benefit.
## 6. Remain respectful of other members at all times
All posts should be professional and courteous. You have every right to disagree with your fellow community members and explain your perspective.

However, you are not free to attack, degrade, insult, or otherwise belittle them or the quality of this community. It does not matter what title or power you hold in these forums, you are expected to obey this rule.

# General Posting Guidelines
We figured this was necessary because a lot of people come in and post threads without thinking, and without realizing that there is no possible way they could get help because of the way they posted the question. Here are some general guidelines.
## 1. Please use SEARCH first!
There is a pretty good chance that unless you have some really odd or unique problem that it has been addressed on our forum before, please use the forum’s search feature first to see if there are already some good threads on the subject. It’s easy to search – just click the “Search” button at the top right of the page.

## 2. Be DESCRIPTIVE and Don’t use “stupid” topic names
PLEASE post a descriptive topic name! Give a short summary of your problem IN THE SUBJECT.  (Don’t use attention getting subjects, they don’t get attention and only annoy people).

Here’s a great list of topic subjects YOU SHOULD NOT POST :

> Help me,  Hello,  Very urgent, I have a question

Generally ANYTHING similar to those is unacceptable. Just post your problem.

Here is a good example of a way to post a question

> “how to calculate the sensitivity of net annual operating cash flows”
or
“where is the ACCA exam centre in Glasgow”

**Remember when people help you, they are doing YOU a favor**
Be patient, help people out by posting good descriptions of what you need help with, and not snapping at people with garbage such as
“if you aren’t going to help don’t waste my time replying”.

# General Forum Questions
## Am I allowed more than one account?
No, there is no reason why you should have more than one account at OpenTuition forums. If you are banned from the forums, please do not create a new account. If you continue to create new accounts after you have been banned, your IP address will be blocked from the forums.

## What happens if I break a rule?
If you break a rule, then you will either be warned or banned. A ban of your user account may either be temporary or permanent.

The administrators and moderators also have the right to edit, delete, move or close any thread or post as they see necessary, without prior warning.

## What happens if I see a thread/post which has broken a rule?
Please report the thread/post to the moderators or admin.

## Where can I find out who is in charge?
The Forum Moderators are displayed in every Forum.

# Becoming a moderator
All moderator applicants must be a member for at least 90 days (3 months) and have at least 100 posts.
You must be active in the individual forums you wish to moderate and regularly create and respond to threads in those forums.
You must also maintain a working knowledge of the subject matter.

Please be aware that applying to be a moderator does not guarantee acceptance and that moderators will only be appointed when needed.

Current moderators and administrators will review applications and decide if the applicant fits the desired post. Administrators have the final say and we reserve to right to refuse applicants with or without cause.
## Moderator Policy
In Order To Apply To Be A Moderator You Must be:

* a forum regular
* have been at the forum for over 3 months
* have a positive presence on OpenTuition Forums
* be proactive
* knowledgeable in the Forums they would like to moderate.
* polite and helpful towards other members and give advice whenever needed and whenever possible.
* visit the forum each day, actively take part in discussions as often as possible, ideally once a day, setting a good example to the other members.
* take an active part in discussions between Moderators relating to the running of the forum.
* help to keep unsuitable content out of the forums as much as possible.
