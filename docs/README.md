# PB50 Network Rules

## Global Rules

:::danger Bans
Bans will be awarded if you get warned 3 times
:::
:::warning Kicks
Kicks are basic warnings used by staff. A kick is a warning but  warning can be used as well
:::

### No Hacking

Hacking or use of 3rd Party Software is strictly NOT ALLOWED. We only allow `Optifine` and `Shaders Mod`

### Be Respectful

Respect all players and staff on the server. We are all Humans after all

### Listen to Staff

Server staff has the ultimate say on the server!

### Do not use foul language

Use of words like `S***`, `F***` and similar is not permitted

### Staff do not handle Staff Applications

To apply for a staff rank you need to watch our discord server or forum.

## Survival

Here are the rules to follow on our survival server

### No Griefing

:::tip How do we define Griefing
Any of the following counts

1. Breaking Claimed Property
2. Lag Machines
3. Attempting to block from Claiming
4. Use of TNT Cannons

:::

### No Great-Spawn-Area Greefing

:::tip What is the Great-Spawn-Area
It is the area around the spawn platform `100` blocks out each way
:::

1. The Great-Spawn-Area is reserved for shops and fan made builds
2. Holes, Towers and other unpleasant structures will be removed
3. Claims with no Buildings will be removed on player request

### Claim Rules

1. Claims can not prevent another player from expanding their build
2. Not claimed areas are not protected by staff. And `Greefing` Reports will be ignored
