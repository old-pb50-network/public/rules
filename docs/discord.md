# Discord Server Rules
* Chat that is abusive towards anyone **(including yourself)** is prohibited. This includes: 
  * Any sort of personal attack or harassment,
  * Defamation,
  * Libel,
  * Threats, 
  * Abuse,
  * And similar.
* Explicit content of any kind is strictly prohibited. Includes but is not limited to:
  * Nudity,
  * Violence,
  * Gore in any fashion,
  * Promotion of drugs and other harmful substances,
  * Promotion of any unlawful behaviors.
* Sending/Linking any harmful material such as:
  * Viruses, 
  * IP grabbers, 
  * Harmware.<br>
  results in an immediate and permanent ban.
* Advertisement of: 
  * Servers, 
  * Player streams,
  * Websites <br>
  can only be done with staff permission unless prior consent has been given.
* Do not attempt to sell or buy anything on the discord server, illegal/fraudulent monetary activities are prohibited outright
* Attempting to release personal information without consent (Doxxing) is strictly prohibited
* Do not spam
* Civility is required in calls, excessive rage will not be tolerated
* Starting, engaging, flaming, or improperly intervening in drama is prohibited, if there is an issue either take it to direct messages or open a ticket with staff
* Unnecessary pinging of any staff is not allowed and is punishable as deemed fit by the staff
* Anyone with the rank of Admin or above should not be pinged during working hours (9:00 - 17:00 PT) unless there’s is an emergency pertaining to the PB50 Network
* If there is an issue requiring the attention of a moderator, please use the appropriate channels to report such violations instead of pinging or DMing
* Staff rulings/warnings/guidance is final and not to be argued against
* If you believe that unnecessary action was taken against you/another person or there was an abuse of power, message a @Manager with a well formatted message describing the situation, context, and any other relevant information that pertains to the situation
* All Discord [Guidelines](https://discordapp.com/guidelines "Discord Guidelines") and [Terms](https://discordapp.com/terms "Discord TOS") must also be followed at any time
